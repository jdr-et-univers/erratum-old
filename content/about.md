---
layout: layouts/home.njk
---

# À propos

Ce site est un petit site de présentation de mon univers {{ metadata.title }}. Il a pour but de vous présenter les différents éléments qui le composent, les différents personnages qui s'y trouve.

Cet univers est mis à disposition selon les termes de la [licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International](http://creativecommons.org/licenses/by-sa/4.0/). Vous êtes libre d'utiliser cet univers comme base pour ce que vous voulez, à condition de repartager dans les mêmes conditions les contenus qui seraient basé sur cet univers. Il est à noter que cette licence ne couvre pas l'inspiration : vous pouvez vous inspirer de certains concepts ou idées sans que ce soit couvert par la licence, dans ce cas vous n'avez pas d'obligation d'utiliser la licence creative commons. 

( A noter qu'utiliser le contenu de ce site pour entrainer une AI n'est pas de l'inspiration #NoAI )

## Crédits

- Site généré grâce à [Eleventy](11ty.org)
- Icone [Night-Sky](https://game-icons.net/1x1/lorc/night-sky.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Icone [Pine-Tree](https://game-icons.net/1x1/lorc/pine-tree.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Icone [Dead wood](https://game-icons.net/1x1/lorc/dead-wood.html) par [Lorc](https://lorcblog.blogspot.com/) sous [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
- Merci à Marcel Dupoignard et mon groupe de JDR pour de nombreux éléments de cet univers
