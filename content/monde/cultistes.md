---
layout: layouts/base.njk
eleventyNavigation:
  parent: Le monde d'Erratum
  key: Les cultistes
  order: 1
---

# Les cultistes

Les cultistes sont une organisation mondiale dont l'objectif est d'éviter les usages déraisonnables de la magie, ainsi qu'originellement de protéger les gens "normaux" des abus possibles de la magie. Leurs objectifs concrêts ont souvent été stable, consistant à cacher les utilisations les plus extrèmes de la magie, et éviter les guerres entre les peuples magiques. Ils ont souvent existé sous forme de confrérie secrète, et les cultistes actuelles descendes notamment de mages fatigués des nombreuses guerres d'Arthur.

Ils sont rivaux avec frontière, sans forcément en être toujours des ennemis. C'est surtout depuis les arrivées des chefs Bérénice Flamel et Paracelse que les deux groupes sont devenu relativement ennemis, mais les cultistes tendent à avoir une vision assez négative de frontière et surtout de leur chef Paracelse, pouvant être vu comme des irresponsables.

Comme frontière, le mouvement est né à la fois d'un ancien mouvement de vénérateur de Lux nommé les arbitres, et d'autres mouvements s'y étant greffé. Les cultistes sont également au courant des forces les plus obscurs qui existent dans leur monde. Ces tentations de les utiliser notamment ne seront pas sans effet... En effet, des groupes tels que le Schisme sont issue à l'origine des cultistes.

## Orientation globale

L'objectif des cultistes est de "protéger le monde des dangers liés au surnaturel", notamment estimant que "les non mages sont naturellement démuni face à la magie, et du coup pourrait utiliser des techniques inconsidéré contre". Ayant traversé le monde à travers les ages, cette organisation internationale à parfois tenter de légèrement infléchir les événements en leur faveur, par leurs actions et des offres de protections contre les créatures puissantes. Cependant, leurs actions sont souvent moins visible que frontière, en tout cas de ce qui est positifs. Ils ont souvent encouragé des révolutions ou renversement face à des forces ayant quelque peu envie de trop manipuler les énergies dangereuses, assassiné des mages profitant de leur pouvoir pour accéder au pouvoir… mais également fait tombé des gouvernements démocratiques ayant fait des expériences qu'ils estimaient dangereux. Ou aidé des pouvoirs obscurantiste, les trouvant préférable aux mages maléfiques.

Contrairement à frontière qui suit le courant, les cultistes ont souvent tendance à y réagir, à tenter de l'endiguer, par la nature même de leur comportement.

## Historique des cultistes

Les cultistes puisent leur origine à la fois des arbitres et des cultes à mystères. Ces cultes était une des dernières résurgences du lien humain-dieu après l'ère des dieux/héros, et étaient constitué généralement d'un dieu unique protégeant un petit nombre de fidèle. Ces dieux étaient souvent en froid avec les autorité divine, décidant contre le fait de faire partie d'une cité célèste, et de ce fait ne vivant pas comme des "vrai dieux". Ce mouvement a gagné en importance en se réunissant autour d'anciens disciple de Merlin ayant pour but de protéger le monde des forces obscurs, notamment de cacher aux yeux du monde certains artefacts divins trop puissant (tel que le Graal).

Ils ont grandement gagné en puissance durant l'ère catholique, notamment par la participation de plusieurs ordre de l'église catholique, dont les Templier, qui estimaient qu'il était du devoir de l'église de protéger les croyants des forces magiques puissantes, souvent vues comme "maléfiques". Plusieurs sociétés secrètes (tel que les Illuminati, depuis interdit par la Bavière puis absorbé dans les cultistes comme leur agence intellectuelle puis récemment un think-tank notamment dans la communication).

Ils ont longtemps en Europe lutté contre la révélation de la magie aux mortel, et pour masquer son existence. Les dernières décénies ont cependant été rudes pour les cultistes : ils ont fortement perdu en influence et en financement, puisque les gouvernements "normaux" n'avaient plus d'intérêt à recourir à leur protection face aux possibles découverte du monde magique. De plus, ils ont longtemps eu moins d'influence dans les autres contrée du monde.

*Nostradamus* a été chef des cultistes suite à la Guerre des Vertue, mais à cédé sa place ensuite à l'aube du XXe siècle. La cheffe des cultistes début 2020 est *Bérénice Flamel*, descendante de Nicolas Flamel.

## Organisation des cultistes

Les cultistes sont organisé en loge, mais peuvent avoir des fonctionnements très différents suivants les lieux. Le centre principal des cultistes est situé dans les îles Kerguelen.

Les loges cultistes peuvent se diviser en quatre grands types de loges, qui ont souvent plus ou moins d'importance dans la politique globale des cultistes, même si l'arivée de Bérénice à baissé cette influence. On peut y trouver les **cultes à mystères**, des loges centrées autour d'un dieu (souvent ennemi des cités antiques) formant une petite religion aidant les cultistes, les **cercles occultes** des cercles de mages cherchant à masquer des secrets magiques en particulier, des **ordres religieux** cherchant à protéger le monde des démons et autres puissances, et des **sociétés secrètes** ayant dans leur but la conservation des puissances magiques.

### Les cultes à mystères

Les cultes à mystères sont parmis les groupes les plus influant des cultistes, et pour cause : leur chef est généralement un dieu. Fondée par des dieux ayant quitté les cité célèste, ces cultes offrent une puissance moins grande qu'une religion organisée ayant des milliers de membres, mais plus stable. Le principe est simple : le dieu ou la déesse offre la protection directe aux membres du culte à mystère, et les membres læ vénère en retour.

Ces cultes ont rejoint les cultistes parce que les cultistes s'opposent généralement à la puissance des grandes cités célèstes.

### Les illuminati et autres sociétés secrètes

Les sociétés secrètes sont généralement divers groupes de penseurs cultistes ayant pour objectif de réfléchir les objectifs des cultistes et la relation du monde avec la magie. Si en effet "protéger des secrets magique" est un but avec lequel les cultistes sont généralement en accord, souvent les conditions exacte doivent être réfléchie par ces groupes.

Une des sociétés secrètes ayant eu beaucoup d'importance jusqu'à la fin du XXe siècle sont les illuminati. Originellement l'un des plus grande groupe de pensée cultiste malgré leur interdiction par la Bavière, les illuminati ont grandement contribué à tenter de moderniser la pensée cultistes, et la faire s'adapter à un monde ou la science gardait en importance. Ils ont également été les premiers à penser que les cultistes devaient se moderniser dans leur communication, devenir plus adapté au monde d'aujourd'hui.

Ils ont cependant perdu en membre après la seconde guerre mondiale, et est aujourd'hui devenu plus une "agence de communication des cultistes", mais sans que ses volontés aient beaucoup d'effet.
