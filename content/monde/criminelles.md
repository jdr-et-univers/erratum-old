---
layout: layouts/base.njk
eleventyNavigation:
  parent: Le monde d'Erratum
  key: Les organisations criminelles
  order: 4
---

# Les organisations criminelles

Dans l'Erratum, de nombreux organisations criminelles existent et utilisent la magie à des fins néfastes, faisant d'eux ce qu'on appelle des "mages obscurs". Nous allons voir ici quelques unes des organisations qui sévissent en Europe.

## Le Nouvel Empire Romain.

Le Nouvel Empire Romain est l'une des principales organisations criminelles qui sévit en Europe. Ce groupe fascisant pan-européen a pour objectif de prendre le contrôle de toute l'Europe, pour recréer un Empire Romain tel qu'ils le fantasme, controlant toute autour de la méditérannée. Pour cela, elle combine l'utilisation de moyens de peur et de pression politique. Cette organisation à comme caractéristique qu'elle contient parmis ses membres à la fois des mortels et des titans, et son territoire se compose plus d'une quinzaine de cité célèste répartie sur l'Italie.

Sa capitale est située dans la *Cité Célèste des Empereurs*, situé aux abords de Rome, dans un univers-bulle. Son chef officiel est le dieu Jupiter, après une petite période ou Appolo Phoebus avait été le dirigeant de la cité célèste, secondé par un *Sénat Latin* composé de familles magiques puissantes et influantes, refusant le pouvoir de l'OIM en Europe. Jupiter estime avoir "repris le titre d'Empereur" après la fin du dernier empereur du Saint Empire Romain Germanique, règle ayant été ratifié par le Sénat Latin (qu'il avait lui-même fondé). Cette organisation mélange des aspects autocratique (l'empereur ayant une très grande puissance), et des aspects oligarchique plus traditionnel (de nombreux aspect de la vie dans l'Empire Romain sont controlée par le Sénat plutôt que par Jupiter). 

Chose unique, les dieu Romain ont offciellement avoir "renoncé au titre de dieu" et "embrassé la religion chrétienne". Ils ne se définissent plus comme des "dieux" (laissant se titre au monothéisme chrétien) mais prenne celui de "titan". Ce geste politique leur a permis de gagner en réputation et en popularité, et d'endomager la réputation d'autres groupes de dieux, les faisant passer pour des "orgueilleux se proclamant plus qu'ils ne le sont". Les dieux romain ont cette particularité d'être les *uniques dieux signés*, mais peu de choses sont connus de leurs origines. Ils servent de généraux à l'armée romaine.

Ils apprécient tout particulièrement les membres forts et résistant. Ce groupe est principalement en guerre face à d'autres cité célèste, et déclare "ne pas avoir d'intention" envers les pays existant. Malgré le decrêt de l'OIM instaurant leur illégalité, de nombreux dirigeant et puissant d'Europe se réfère à eux.

## Le schisme

Le Schisme est un groupe qui s'est séparé des cultistes et de frontière suite à la guerre des vertues, parce qu'ils estimaient qu'il faudrait être plus radical dans l'utilisation de la magie et avaient trop peur d'utiliser les forces les plus dangereuses (mais énergétique) tel que le Néant et l'Anomie. La plupars des grands incidents de magie obscurs du début du XXIe siècle sont du à eux, mais il y a des débats pour savoir s'ils sont véritables plus actifs que les Chasseurs ou le Nouvel Empire Romain, ou si c'est leurs méthodes qui sont plus dans la norme de ce qu'on appelle de la magie obscure.

Peu est connu cependant du schisme : il s'agit d'un groupe extrèmement secret, et dont bien des membres ont été corrompus par les énergies qu'ils utilisent. Les quelques éléments les plus connus de leurs actions sont :

- Ils sont dirigé par un mystérieux homme portant toujours un masque de plâtre, ancien signe de l'armée du dieu *Akash*, principal dirigeant de l'armée unie des dieux lors de la *Guerre des Vertus*

- Le fait qu'ils aient une forte propention à s'attaquer aux dieux et autre créatures extrèmements puissantes.

- Des rumeurs disent que leur objectif serait de permettre à toute l'humanité de s'élevé aux rangs de dieux, mais d'autres diraient qu'ils essaient d'eveiller un mal ancien.

Ils sont ennemis à la fois des cultistes, de frontière… et même des autres organisations criminelles.

## Les chasseurs

*Les chasseurs* sont un groupe criminelle présent partout à travers le monde. Il s'agit d'un groupe initialement composé de non-mage choqué de la révélation magique, ayant décidé de s'organiser en groupe armée pour se défendre contre « les créatures monstrueuses » magiques. Ce groupe se voit comme les chasseurs de dragons de jadis, et sont persuadés que les créatures magiques risque de finir par faire la guerre aux humains et les exterminer. Ils sont d'autant plus actifs contre les créatures considérées comme « presque humains » tels que les Lycanthropes et les Hybrides.

Ils fonctionnent de manière clandestine, et sont officiellement totalement illégaux et combattus. Leurs noms de codes sont généralement basé sur des grands chasseurs/héros/chevaliers... Aux USA, les noms de grands "héros" de l'ouest sauvage sont particulièrement appréciés.

Ils sont parfois employés comme mercenaires par des puissants pour des fins privées. Ils font également du trafique d'arme et de créatures magiques.