---
layout: layouts/base.njk
eleventyNavigation:
  parent: Titans et divinités
  key: Les démons
---

# Les démons

Les démons sont une espèce de créature étrange, proche des demi-dieux, mais sans l'aspect "accouplement entre humain et dieu". On peut les considérer comme une forme de "sous-titans" (des sortes de titans incomplet naturels, non hybridés), dont les pouvoirs seraient les égaux plus de ceux des demi-dieux.

Les démons peuvent provenir de *titans déchus*, dont les pouvoirs ont été diminués à partir d'un pouvoir. De nombreux titans du moyen orient ont subit ce sort lorsqu'est apparu la République des Anges, des titans vaincus par les anges ayant été déchus au rang de titans.

## Les enfers

Une grande partie des démons sont enfermés sont enfermé dans les *Enfers*, l'un des deux royaumes de morts de la république des anges. Ils y forment la grande parties de démons connus.

Les enfers sont séparé en neuf cercle, chacun ayant un rôle différent :
- Les **Limbes** sont le premier cercle, d'immense. C'est ici que résident tout ceux qui sont arrivé en enfer, mais qui ne sont pas considéré comme capable d'aider les démons. C'est un lieu plutôt vaste, avec quelques villes. C'est un endroit également de grande lassitude. En gros, on s'y fait un peu suer.
- La **mer des naufragés** est second cercle des enfers, une immense mer circulaire entourant l'île principale des enfers. L'ile
- Les **Plaines Désolées** sont le troisième cercles. D'immense plaine ou ne poussent que des aliments sans saveurs, il s'agit d'un endroit morne et sans évenement, ou se trouve de nombreux petits village. Cela reste l'un des cercles les moins dangereux des enfers, avec les limbes. C'est généralement le dernier arrêt sûr avant les trois prochain cercle, nommé "le chemin des damnés", ou de nombreux spectres ambitieux se perdent en voulant rejoindre le Pandémonium.
- Le **Désert Infernal** est le quatrième cercle. C'est un immense désert sablonneux, qui doit être traversé pour atteindre les cercles suivants des enfers. De nombreuses créatures dangereuses y vivent.
- La **Grande Fosse** est le sixième cercle des enfers, un immense fossé traversé par 9 ponts. Le fond de la fosse est un endroit boueux, ou le dégout reigne. De nombreuses personnes y vivent cependant, et on raconte que c'est ici que se forment tout les complots contre Satan.
- Les **Monts du Trépas** sont l'immense chaine de montagne sur laquelle repose le Pandémonium. Ses monts sont abrubts et de nombreuses coulées de boue et de magma s'y trouvent.
- Les **fortifications du Pandémonium** forment le septième cercle des enfers. Ces immenses fortifications successives s'étendent sur des kilomètres, avec des villes entre chaque lignée. Ces villes sont proche du Pandémonium, mais sans la présence des quartiens luxueux. La guarde y est de plus en plus présente, rendant difficile les infiltrations.
- Le **Pandémonium** est la capitale des enfers, et forme le huitième cercle des enfers. Il est réservé à l'élite des démons, mais aussi aux morts les plus prestigieux… ou ceux qui arrivent à l'attendre malgré toute les épreuves. Il s'agit d'une ville pleine de violence, mais ou il est possible de grimper dans la hiérarchie des enfers.
- Le **Grand-Palais Infernal** (ou Palais de Satan) est situé au centre de la ville et forme le dernier cercle.

Les démons des enfers étaient initialement dirigée par sept princes : Lucifer, Mammon, Asmodée, Léviathan, Bélzébuth, Satan et Belphégor. Cependant, avec l'aide de Lucifer, Satan à fait un coup d'état pour devenir le dieu des enfers, faisant en sorte que les autres prince soient sous ses ordres. Asmodée est même devenu un *démon goétiens* dans sa quête de pouvoir.

Les démons des enfers embauches les spectres amenés en enfers pour former leur armée, pour pouvoir un jour se venger des anges lors de la *Dernière Bataille*.

## Les démons goétiens

Les démonts goétiens sont des démons ayant fait un pacte avec l'espèce humaine, leur accordant leur pouvoir en échange de confiance ou de crainte. Grace à ce pacte, ces démons peuvent, comme les titans de troisième génération, monter en puissance plus de gens dépendent d'eux.

Cela à souvent été vu comme une manière de *donner son âme* aux démons, du à certaines personnes ayant même promis que leur spectre rejoindrait l'armée du démon pour la *Dernière Bataille*. Mais la plupars du temps, cette idée de "vendre son âme" doit être prise au sens figurée, puisque faire ça augmente la puissance des démons pour cette "dernière bataille".

Ces démons utilisent une forme de magie démoniaque pour pouvoir faire apparaitre des instances d'eux sur Terre, capable d'aider les gens appellant leur aide… Cependant, ils restent des démons… ce n'est pas forcément facile de les faire ce qu'on veut.

Il existent de nombreux démons aidant la Goétie.
