---
layout: layouts/base.njk
eleventyNavigation:
  parent: Titans et divinités
  key: Royaumes des morts
---

# Royaumes des morts

Après la mort d'un individu, il peut laissé une *emprunte* dans l'énergie magique environnement, en cas de présence de forte énergie magique. Cette emprunte, généralement nommée *spectre* ou *fantôme* peut peut apparaitre dans les jours, semaine voir année après le déces. Contrairement à des croyances pendant de longue époque, cette emprunte ne serait pas "l'âme" du défunt, mais un phénomène de réminiscence

Les spectres peuvent parfois être dangereux, apparaissant particulièrement dans les cas de morts violentes, et provoquant naturellement de la peur. Durant l'ère des dieux, le rôle des dieux dans les société étaient de protéger les humains des dangers que pouvaient représenté les spectres. Les dieux amenaient alors les morts dans des univers-bulles nommé "Royaume des morts".

Aujourd'hui, ce rôle est joué par des individus particuliers.

## Les spectres

Un spectre est une emprunte laissé par un défunt, gardant généralement les souvenirs et les particularités de son caractère. Elle est immatérielle, et donc traverse et est traversée par les objets physiques, et donc ne peut affectuer directement de cette matière.

Cependant, il peut l'affecter *magiquement*. Premièrement, les spectres peuvent produire un *champ de peur*, un espace ou les humains sont plus sensible à la peur, et donc à ses effets néfastes. Ils produisent naturellement aussi des interférences avec les appareils éléctrique : attendez vous a voir des ampouilles tressaillire, ou votre télé avoir des interférence sur l'image. Ce champ est d'autant plus présent que l'esprit est agité, un esprit calme en produit peu, voir aucun.

Les spectres peuvent affecter aussi volontairement ce qui les entoure, en prennant possession des choses matérielles.

Les spectres ne peuvent ni changer ni évoluer. Ils continue d'exister tant qu'ils ont une source d'éclat à proximité. C'est pour cela que certaines maisons sont particulièrement propice à cela, quand elles sont sur des *courant telluriques*.

## Royaumes des morts

Les royaumes des morts sont des univers-bulles dans lesquels les fantômes et les spectres peuvent résider après leur mort. Ils sont souvent gardé par des dieux, et les spectres arrivent souvent dans le royaume des morts auquel ils s'attendent (ou au plus proche le cas échéant, ou dans des circonstances spéciales).

Ces royaumes sont des lieux hautement magique, et les spectres y restent donc pour l'éternité. Chaque royaume à ses propres règles.

Ils sont dans la plupars des cas interdits au mortels, mais il arrive que certains soient entrés pour découvrir des conseils ou demandé conseil.

## Les morts (ou faucheuses)

Les morts sont des titans s'occupant de rechercher les spectres pour les amener en enfer. Les spectres "paisible" peuvent choisir de rester, mais les agités sont souvent amené en enfer. Les Morts sont nombreuses, mais ne le sont pas assez pour récupérer tout les spectres dès leur apparition, et généralement apportent s'occupent en priorité des cas qu'elles estiment le plus important.

Elles ne sont généralement visible qu'au défunts, et ont une apparence différente suivant les panthéons auquels elles appartiennent.

## L'exorcisme

L'exorcisme est une pratique consistent pour des humains à jouer un rôle smilaire aux morts. L'exorcismes s'est d'autant plus répendu qu'avec l'augmentation de la population.

Il existent de grande famille ou écoles d'exorcistes un peu partout dans le monde, dont :
- La famille britannique *Patterson*, utilisant généralement une magie nommée les Mirage pour combattre les spectres dangereux.
- La famille quebecoise des *De L'Étincelle*, utilisant principalement des armes magiques liées à la lumière et l'ordre, et possédant l'épée légendaire *l'Étincelle*
- La famille *Hososhi*, au Japon, descendant d'un exorciste antique devenu Yokaï, experte en utilisation de sceau pour emprisonner les démons et esprit malfaisant.
- Les membres de l'école d'exorcisme de *Zhong Kui* en Chine.
- *L'académie du Soleil* à Cusco forme des exorciste selon les traditions Inca.

Une grande partie des ordres ou traditions religieuse ont également leur tradition exorciste, et forme une grande partie des exorcistes.
