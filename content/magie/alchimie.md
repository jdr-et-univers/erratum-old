---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les pratiques magiques
  key: L'alchimie
  order: 2
---

# L'alchimie

L'alchimie est une pratique ancestrale, s'étant développé sous des noms différents dans de nombreuses contrées du monde. Si l'alchimie est souvent présentée sous deux angles, la création de la Pierre Philosophale et la transmutation du plomb en or, cette discipline est en fait plus complexe.

L'alchimie n'est pas en soi une *magie*, mais une meta-magie. L'alchimie est le travail des pratiques magiques à l'aune des connaissance de la métaphysique appliquée, c'est à dire l'étude et l'application des théories métaphysiques sur le fonctionnement du monde. Dis autrement, l'alchimie vise à obtenir la compréhension du monde tel qu'il est (le noumène) comparé à ce qui nous est apparant et accessible (le phénomène), et à pousser les pratiques magiques à leur paroxisme via l'application de cette connaissance

Cette pratique peut se révéler dangereuse pour l'esprit, parce qu'elle porte dans des choses que notre esprit n'est pas fait pour comprendre. Elle est notamment connue pour provoquer beaucoup de fatigue morale et psychologique, demandant du repos.

Parmis les plus célèbre alchimistes se trouvent Nicolas Flamel, Paracelse Ier et le dieux Hermès qui a le titre du Trismégite en Europe pour cela. De nombreux alchimistes ont existé dans d'autres civilisations, et peut-être que la Pierre Philosophale de Nicolas Flamel est simplement l'unique encore en activité.

## Les pratiques alchimiques

La principale méthode de fonctionnement de l'alchimie consiste à la maitrise et l'amplification des pratiques magiques. Dans l'alchimie, chaque pratique magique est représenté par un élément, et ces éléments sont "modelé" pour atteindre des nouveaux paroxismes.

Un des exemples est la pratique d'une magie élémentaire qui toucherait les éléments métaphysiques, tels que l'ombre et la lumière.

## La Pierre Philosophale

La Pierre Philosophale est un objet célèbre de l'alchimie. La Pierre Philosophale est un objet qui consiste simplement en l'abolition des règles de la magie. La Pierre Philosophale est un paradoxe, offrant des réserves illimitées d'éclat, et une vision du monde tel qu'il est presque illimitée.

L'unique pierre philosophale recensée est celle crée par Nicolas et Perenelle Flamel. Plusieurs autres existe de manière légendaire, tel que celles construite dans la *Cité d'Or* aux Amériques, ou le légendaire *Livre Noir* dont les contes racontent que le fils du créateur serait emprisonné dedans, et fournirait son pouvoir à qui le trouverait.

Flamel lui-même blague souvent sur l'idée que le vrai Flamel serait peut-être en fait mort et qu'il ne serait que la Pierre Philosophale répliquant ce qu'il restait de lui.

## L'entropie

L'entropie (ou entropie magique) est un phénomène théorisée par les alchimiste. Sa définition est quelque peu différente de l'entropie telle qu'exprimé en physique.

En effet, l'entropie désigne ici l'entropie de l'énergie magique environnante, c'est à dire son degré d'imprédictibilité et la quantité d'information. Cependant, la quantité d'information affectant la magie est tout simplement immense : chaque donnée physique, métaphysique ou ne serait-ce que pouvant être posée en hypothèse fait partie de l'information magique. Dis d'une autre manière, l'entropie magique représente les différences configurations possible de l'univers.

Naturellement, l'entropie magique à tendance à s'uniformiser. Cependant, c'est là où peut arriver une *modification de l'entropie*, se rapprochant de la notion de hasard tel que présente dans l'informatique. En effet, la modification de l'entropie magique est l'ajout d'un élément extérieur dans le système magique, c'est à dire la création d'une *conséquence sans cause*. Par effet boule de neige, chaque information pouvant être lié aux autres dans un système magique, sans forcément de liens "physique" mais parfois juste conceptuel, une modification de l'entropie peut avoir des effets impressionnant.

Les légendes racontent que le peuple des anostiens avaient une certaine maitrise de l'entropie.
