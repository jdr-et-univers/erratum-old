---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les pratiques magiques
  key: Les écritures magiques
  order: 1
---

# Les écritures magiques

Les écritures magiques sont des types de magies très particulières, nombreuses, utilisant le pouvoir des mots et surtout des écritures. Ces types de magies se base sur la puissance créatrice existant naturellement dans les mots, les écrits ne faisant que donner des instructions à ces magies.

Ces types de magies sont aussi ancien que l'écriture elle-même, et sont encore très utilisé aujourd'hui pour créer des sortilèges durables, notamment par les *alchimistes*. De nombreux grands sortilèges existent dans le monde et sont étudié par les spécialistes, et servent notamment à faire fonctionner des sortilèges supposé durer longtemps.

## Sortilèges pré-conçus

Les sortilège préconçus sont des sortilèges qui ont été conçu et adapté précisément à partir d'une écriture magique - voir plusieurs, même si le mixage d'écriture magique peut donner parfois des résultats inattendu pour qui ne connais pas, à cause de "soucis de traduction", de concept n'existant pas forcément dans la conception de l'autre langue.

Si tout le monde ayant une certaine maitrise de son éclat peut activer un sortilège, pré-conçus, les construire demande une maîtrise plus grande cette fois de l'écriture magique en question.

Depuis le début de l'ère contemporaine, le conglomérat Frontière travaille sur des sortilège préconçus informatisé accroché à une batterie à éclat.

## Les runes

Les runes sont un des systèmes magiques ayant le plus regagné en intérêt, notamment depuis le succès dans le monde magique des histoires parlant de la mythologie nordique et germanique.

On trouve des runes un peu partout, certaines actives et d'autres non.
