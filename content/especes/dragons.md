---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les espèces d'Erratum
  key: Les dragons
  order: 5
---

# Les dragons

Les dragons sont une espèce intelligente reptile non-sociale, indépendante qui vit dans de nombreux territoire du monde. Souvent considéré comme des "bêtes" ou au moins comme n'étant pas une civilisation de leur nature isolationiste, se sont des êtres intelligents doué de paroles, doté de cultures riche et communicant entre eux.

Ces espèces gigantesques sont magiquement très puissantes, mais ne peuvent utiliser de signes ou des magies comme les civilisations mortelles. On raconte que les dragons sont plus proche niveau puissance d'un demon/demi-titan que d'un mortel. Ils peuvent vivre des centaines d'année, et un oeuf de dragons met des années à éclore. Les dragons sont tous lié à un élément, généralement lié à l'environnement ou ils vivent. La sous-espèce du dragon affecte fortement de quels éléments ils peuvent être.

Les dragons sont présent sur tout les continents, et sont divisé en deux grandes espèce : les dragons-serpents et les dragons-griffus. Les relations entre dragons et humains ont été très différentes suivant les civilisation.

## Dragons griffus

Les dragons griffus sont une espèce de dragon quadripède, dôté d'une grande paire d'aile. Ils sont très présent en Europe, Moyen-Orient, Afrique et dans les cercles polaires. Doté d'écailles, de grande corne, ils sont imposant et impressionnant.

Ces dragons sont très différents suivant les régions, et possèdent de nombreuses sous-espèces :
- Les dragons des plaines sont les dragons que l'on retrouve le plus en Europe, et dont quelques membres sont présent aussi en amérique du nord. Ils sont connue pour leur capacité à cracher du feu. Ils sont généralement de couleur vive.
- Les dragons marins vivent sur les rochers dans les mers agités. On en retrouve de nombreux en méditéranné, en atlantique, mais également dans la mer rouge. Ils crachent un venin corosif, et peuvent nager sous l'eau. Ils sont généralement d'une teinte entre le bleu, le vert et le gris.
- Les dragons du désert sont des dragons couleur sable, se terrant souvent dans les déserts et les montagnes. Ils sont très nombreux dans le désert d'arabie, le Sahara et en Espagne. On en trouve quelques uns aussi dans les désert américain. Ces dragons cherche souvent de l'ombre pour s'abriter.
- Les dragons des glaces sont des dragons de couleur bleu clair ou blanc, vivant dans les cercles polaires. Ils ont un souffle glacial.

Ces dragons sont connus pour leur tendance à "garder des trésor". En vérité, c'est plutôt un effet de la territorialité des dragons. Ils ne gardent pas le "trésor", mais leur lieu de vie - et souvent leur dragonneau. En effet, un dragonneau griffu reste vulnérable pendant 20 ans, ce qui fait qu'à tout moment, ils peuvent être tué durant cette période.

## Dragons serpents

Les dragons serpents sont des longs dragons sans ou avec de petites pattes, vivant à travers le monde. Ces dragons peuvent voler sans ailes, un phénomène encore non-expliqué. Elles est recouverte d'écaille, mais peut également avoir des plumes voir des poils. Cette espèces vit principalement en asie, même si certains sont présents aussi sur le territoire américain et en afrique.

Ces dragons sont généralement considéré comme en trois grandes espèces :
- Les dragons-serpents des airs. Ces dragons ont la capacité d'influencer la météorologie, pouvant appeller des pluies bienfaitrice ou destructrice, ainsi que la foudre ou le vent. Cette puissance fait qu'ils étaient vénéré, notamment en Chine ou seul l'empereur pouvait porter ses signes sur ses vêtements. En mésoamérique, les serpents à plume sont de cette espèce.
- Les dragons-serpents des terres. Ces dragons vivent souvent proche des cours d'eau, et peuvent les déplacer à leur guises, pouvant irriguer des zones entières. Ces dragons ont des migrations, passant l'été dans le ciel et l'automne dans la mer, et le reste de l'année sur terre.
- Les dragons-serpents sousterrains. Ces dragons ont la capacicté de façonner les terrains, de creuser des vallées. Malgré leur nom, ces dragons peuvent aussi voler. Ils sont des protecteurs des trésors des profondeurs, un comportement présent aussi chez les dragons griffus.

Leur apparence peut grandement varier suivant ces trois espèces, les dragons étant plus une espèce "magique" que fonctionnant suivant les règles classiques de la biologie.

Leurs mues ont des capacités curatives.

## Les kobolds

Les kobolds sont des serviteurs des dragons. Ces petits êtres reptiliens bipède vivent souvent dans les carvernes des dragons, et les aident dans la vie de tout les jours. Ils sont souvent considéré comme peu intelligent, mais cela tiens plus du cliché qu'autre chose. Ils n'ont été reconnu comme espèce sentiante que vers la moitié du vingtième siècle. Ils ont souvent des pouvoirs élémentaires lié à ceux de leur dragon.

Ils ont presque disparus pendant un moment en Europe à cause de la chasse au dragon, était souvent attaqué parce que plus simple à attaquer qu'un vrai dragons. Certains tueurs de dragons ont largements utilisé des os de kobold pour gonfler leur score, les crânes de kobold pouvant ressembler à un crâne de dragonneau, tout en étant largement moins dangereux.

Il y a souvent eu des débats sur s'il fallait considérer ou non les Kobold comme membres du petit peuple.