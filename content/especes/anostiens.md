---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les espèces d'Erratum
  key: Les êtres de magies
  order: 4
---

# Les êtres de magies

Les êtres de magies pures sont des êtres composé d'éclat, dont le corps est une forme de projection, imitant les caractéristiques de la matière. Proche sur de nombreux aspects des *fantômes*, mais ayant des structures plus fiable et plus solide, ils peuvent être également assimilés aux *anges et aux diablons*, ayant beaucoup de leurs caractéristique (être assexués, être composé de magie pure, etc).

Parmis ces êtres on retrouve les Anostiens, les changeformes et les esprits domestiques… ainsi qu'une immense quantité d'esprits locaux différents suivants les régions du monde !

## Anostiens

Les anostiens étaient le quatrième des quatre grand peuple, vivant sur les Terres de Lémurie, aujourd'hui connu sous le nom de plateau des Kerguelen, ils étaient sous la protection du dieu *Yule*. Êtres bleu.e.s assexué.e.s, à la queue de "démon" et avec des cornes extrèmement sensibles (pouvant notamment connaître la pression de l'air et tout) ainsi que d'ailes d'éclat pur, les Anostiens sont auréolés de mystères.

En effet, iels avaient la particularité d'exister en nombre fixe : à la disparition d'un Anostiens, un autre. Le temps ne fonctionnait pas de la même manière, leur espérance de vie ayant un caractère pouvant sembler aléatoire pour les autres peuples. De plus, si leur vieillissement fonctionne avec les mêmes phase que les autres peuples, la durée semble erratique.

Les anostiens ont disparu il y a 10 000 ans, environs en même temps que se sont effondré les civilisations antiques.

## Les changeformes

Les changeformes sont des êtres polymorphes, connus pour leur pouvoir de ne pas avoir d'apparence spécifique et de pouvoir prendre celle qu'ils veulent. Ils ont presque disparu lors de la *guerre des vertues*, mais ont réussi à subsisté dans certaines régions du monde. Ces esprits ont une durée de vie proche de celles des espèces humanoïdes.

Peut est connu de ces êtres, si ce n'est qu'ils ont le pouvoir de se lier émotionnellement à d'autres êtres.

## Les esprits domestiques

Les esprits domestiques (aussi appellé "peuple des foyers") sont des êtres de magie pure, formée par la magie environnante présents dans certaines maisons. Ils sont les protecteurs de la maison, et les bienfaiteurs de la famille qui y vit. Cependant, il est important de savoir que la plupars du temps, les esprits domestiques n'ont pas de "loyauté" magique à la famille, mais uniquement *à la maison*. Cela ne veut pas dire qu'ils ne peuvent pas avoir de l'amitié (ou de l'inimitié) envers la famille qui y vit.

Parmis les esprits domestiques on peut trouver les lutins, les lares, les nisses, les brownies est bien d'autres créatures dont les pouvoirs et les capacités changent suivant les yeux. Il est à noter que parfois, les esprits domestiques se forment à faire des volontés de défunts qui ont vécu dans la maison (dans ce genre de cas, ils auront souvent un attachement plus grand à leur famille qu'à leur maison)

Les esprits domestiques sont souvent prêt à aider pour certaines tâches, mais doivent être traités avec le plus grand respect. En effet, ils sont parfois farceurs, et n'hésiteront pas à se venger si jamais on les trompe ou abuse de leur hospitalité… Par contre, leur faire des farces est souvent acceptable… si on est prêt à en avoir en retour !

Les esprits domestiques font partie du petit peuple.