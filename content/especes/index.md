---
layout: layouts/base.njk
eleventyNavigation:
  key: Les espèces d'Erratum
  order: 2
---

# Les espèces d'Erratum

Les espèces d'Erratum sont nombreuses et classifiées en un certain nombre de groupes :

- Les **hominines** sont les descendants des homo sapiens, s'étant adaptés à différents milieux et conditions, ou ayant été affecté par diverses magies.
- Les **fées** sont des êtres végétaux, fortmeent influencés par l'état naturelle et la force magique environnante.
- Les **morts-vivants** sont des corps ramenés à la vie et animés par une magie ancienne.
- Les **êtres de magies pures** comme les anostiens sont des êtres composé à 100% d'éclat, qui créé un corps physique dans lequel ils peuvent se mouvoir.
- Les **dragonoïdes** sont des reptiles anciens et intelligent, souvent dotés d'ailes.
- Les **minéraloïdes** sont des êtres minéraux, composé de roche se mouvant grâce à la magie, tel que les troll.

## Les quatres anciens peuples

Les humains, danaïtes, vampires et anostiens sont appellés les *quatres peuples anciens*, qui auraient formé le coeur des *quatres grandes civilisations antiques*. Ces espèces ont été décrété selon la tradition comme ayant "règne sur toutes leurs espèces apparentées". Cela fait que pendant de nombreux temps, les grandes réunions des "peuples anciens" contenant les trois premiers peuples (les anostiens étant quasiment éteint), laissant hors de la boucle les autres peuples.

Cette notion est évidemment aujourd'hui fortement contesté, même si dans chaque peuple il existe des nostalgiques de cette notions, et qui la considère comme pleinement valide. C'est à cause de cette notion que des êtres comme les loup-garou et les ondines ont longtemps manqué de droits. C'est aussi suivant ces notions que le petit peuple est souvent déconsidéré, et que les dragons n'ont pas été considéré comme un "peuple" mais des "créatures".

## Le petit peuple

Le petit peuple est un ensemble de petits êtres magiques souvent lié à des lieux ou des espaces, mais qui peuvent faire partie de plusieurs des grands groupes d'espèces cités ci-dessus. Parmis les membres du petit peuple, on peut trouver :

- Les *peuples des forêts* (et des plaines) de la famille des fées et danaïtes
- Les *esprits domestiques*, de la famille des êtres de magies pures et anostiens.
- Les *peuples des montagnes*, de la famille des minéraloïdes et trolls.