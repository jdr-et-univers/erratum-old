---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les espèces d'Erratum
  key: Les minéraloïdes
  order: 6
---

# Les minéraloïdes

Les minéraloides sont des créatures minérales ressemblant à des sortes de reptiles anthropomorphiques de pierre et de roche, vivant principalement dans les montagnes et se nourrissant des sédiments.

Ils sont composé de quatre "espèces" : gobelins, trolls, orcs et géants. Cependant, le mot "espèce" est faux, il s'agit des stades de l'évolution d'un minéraloïdes. Cette évolution d'un individu - ou plutôt d'un groupe d'individu - est particulière, parce que se fondant par collision - comme les astéroïdes en quelques sortes. Les minéraloïdes se cognent entre eux dans de grandes mélée, et peuvent fusionner vers un stade supérieurs. Ces stades ne correspondent pas non plus à nos ages, puisqu'un minéraloïdes n'atteindra en effet pas forcément (et même rarement) les derniers stages.

Les minéraloïdes n'ont ni véritable structures sociales, ni légendes, ni n'ont formé de "grande civilisation puissante". Et ils le vivent super bien. Ils seraient plus vieux encore que les quatre grande civilisations. Les différents stades ont des tendances leur donnant parfois des choses vues comme des "roles" par les observateurs extérieurs, mais il y a en vérité beaucoup de variations. Cependant, il serait faux de dire qu'ils n'ont pas de culture. La culture minéraloïdes est très riche, et différente de montagne en montagne, mais n'aurait simplement pas besoin d'une société "structurée" ni de légende pour cela.

C'est l'espère pour laquelle la magie est plus rare, encore plus que pour les homininé.

## Gobelins

Les gobelins sont le premier stade de l'évolution troll. Ils se forment à partir de collision de géant ou d'orcs. Les gobelins sont de petite taille, de 70cm à 1m.

Les gobelins sont souvent considéré comme surexité, violents et bagareurs. Ils creusent avec une facilité déconcertantes, la plupars des grandes groupes de minéraloïdes vivent dans des galleries créé par des gobelins en creusant. Cependant, les gobelins n'ont pas de "rôle" de creuseurs : ils creusent parce qu'ils aiment ça.

Les gobelins et le petit peuple des montagnes sont souvent ensemble pour faire des farces à des aventuriers peu attentif dans les montagnes. Souvent avec des trous creusé dans le sol. Les gobelins sont souvent vu comme des créatures "semi-intelligente" par les humains, le même genre de préjugé porté sur le petit-peuple.

Les grandes mélées de gobelins provoquent souvent la création de trolls.

## Troll

Les trolls sont le deuxième stade de l'évolution. Ils sont relativement nombreux, même si un peu moins que les gobelins. Cependant, ils sont les plus connus, étant de ceux qui sortent généralement des montagnes. Ils mesurent entre 1m70 à 2m

Ils sont capable de reconnaître n'importe quel pierre, de trouver des gemmes et des métaux rares, le tout sans avoir de mines industrielles. Ils sont d'excellent forgerons et bijoutiers, et les outils trolls sont considérée comme les meilleurs qui existent.

C'est le stade vu souvent comme le plus "civilisé" de l'évolution, notamment par les homininés… sans doute en grande partie parce que c'est ceux les plus proches des humains.

Les grandes mélées de gobelins peuvent parfois provoquer la création d'orcs.

## Orc

Les orcs sont le troisième stade de l'évolution troll. Vu comme rare, ils sont cependant néammoins un peu connus, et sont souvent engagé comme garde du corps hors des montagnes. Pas tellement plus grand que les trolls, ils mesurent entre 2m et 2m50, mais ils sont costaud et massif.

Les Orcs sont souvent considéré comme les "guerriers" des minéraloïdes, de par leur force et leur puissance encore plus grande que celle des trolls. Cependant, ils n'ont pas spécialement de propension à cela plus qu'être fort et solide. Ils sont d'un naturel bien plus calme que les troll et surtout les gobelins, et parlent moins. Cependant, ils sont tout aussi intelligent que les autres stades.

Les grandes mélées de gobelins peuvent parfois provoquer la création d'un Géant, et de plusieurs petits gobelins.

## Geant

Les géants sont le stade finale de l'évolution des trolls, et sont extrèmement rare. Ils restent généralement dans les montagnes, et sont les gardiens de tout les minéraloïdes de la montagne. Ils mesurent généralement plus de 4 mètres, et peuvent atteindre des dizaines de mètres.

Les géants ont longtemps été cru comme étant les "roi" des minéraloïdes… Dans les faits, ils ne le sont pas, et les minéraloïdes vivent souvent sans vraiment se préoccuper du géant de la région. Les géants parlent très peu, et passent une grande partie de leur temps à dormir. Ce sont des êtres puissants, et ceux qui les attaques ont souvent découvert tristement qu'ils ne font pas le poids.

Les légendes disent que quand un géant parle, ils utilise des métaphores minérales que seules les autres minéraloïdes peuvent comprendre.

Les géants entrent en collisions entre eux, souvent quand ils y a deux géants dans le même territoire. Cela peut provoquer parfois soit la création d'un géant plus grand et de gobelins, soit l'éclatement de l'un des deux géants en une multitude de gobelins et de trolls. Et souvent, à partir de cela, le cycle peut reprendre.

## Peuple des montagnes

Le peuple des montagnes est le noms donnés à de petits êtres minéraux vivant souvent en montagne, et né des collisions entre géants. Trop petits pour devenirs des gobelins, ils sont extrèmements magiques et ont souvent une façon de vivre plus souterraines. Les plus célèbres sont les gnomes, des petits êtres souterrains se nourrissant de silicates, et les nains, connus pour leurs grandes barbes de fibre de silicium.

Ces êtres vivents dans des grande galeries et exploitent des filons de minerais, dont certains qui ne leur sont visible que d'eux.

Le peuple des montagnes fait partie du *petit peuple*.