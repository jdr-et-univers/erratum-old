---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les espèces d'Erratum
  key: Les mort-vivants
  order: 3
---

# Les mort-vivants

Les "morts-vivants" sont le noms donné à des êtres dont le corps est mort, mais préservé et maintenu en vie et état de fonctionnement par une magie extrèmement puissante.

Les plus célèbres sont les vampires, les uniques morts vivants capable de se "reproduire". A l'exception de cela, les morts vivants doivent forcément être créé par une magie puissante, ou un événement. Certaines forces magique de vie pure peuvent provoquer des morts-vivant.

Les morts-vivants sont souvent considérer comme n'ayant pas (plus) d'âme, celle-ci aurait "fait son chemin" après la mort du mort-vivant. Cependant, depuis le 20e siècle, cette idée est fortement contestée, à la fois par des religieux estimant que l'âme n'arrive "pas encore à dieu" mais également par tout ceux ne croyant simplement pas en l'idée d'une âme détachée du corps. L'absence de création de spectre lors d'une transformation en mort vivant est souvent vu comme un indice, bien que les spectres ne sont pas l'âme d'un défunt.

Il ne faut pas confondre les morts vivants avec les spectres, les deux étant des processus différents, bien que s'excluant mutuellement.

## Vampires

Les vampires sont une espèce particulièrement rare, nés de la civilisation antique de *l'Hyperborée*. On raconte que *Mabon*, leur dieu, est celui qui leur aurait offert l'immortalité, faisant d'eux des morts-vivants. En effet, les vampires sont des corps morts, maintenu par une magie extrèmement puissante. Le sang ne coule plus dans leur corps, ils sont incapable de se reproduire et ont besoin de sang pour maintenir le sortilège.

Pour se reproduire, ils sont obligé de "vampiriser quelqu'un", le transformer en vampire par une morsure, transformant n'importe quelle autre espèce en vampire, lui faisant perdre ses traits naturels. Ils ont des pouvoirs de contrôles puissant, pouvant transformer des êtres en ghoule. Ils ne sont cependant pas totalement invulnérable. Le soleil les affaiblis, ils sont allergique à l'ail, et peuvent parfaitement être tué (et pas juste besoin d'un pieu dans le coeur).

Si les vampires ont longtemps eu la réputation d'être des chasseurs de proies invétérés, en grande partie en Europe lié aux comtes Vlad Dracula et Friedrich Orlock (dit « Nosferatu »). Le premier est un individu controversé, à la fois fondateur de l'une des plus grandes société de vampire, et despote sévère qui n'hésitait pas à tuer ses ennemis. Il a été tué par l'également controversé Professeur Abraham Van Helsing, professeur d'université devenu médecin puis chasseur de vampire. Le comte Nosferatu est lui une figure négative à la fois pour les non-vampire et vampire. Cet ancien élève de Dracula, vampirisé par lui, a vécu jusqu'à sa mort en 1927. Il est tristement connu pour avoir fondé une brigade fasciste vampirique en 1920, commençant des vampirisations de masse avec ses suiveurs. En 1922, une coalition se fait de vampire et d'humain contre le fascisme, et méneront une guerre de 5 ans contre lui qui finira par sa mort.

Cependant, la majorité des vampires ont été vampirisé sur leur demande, par un vampire mystérieux uniquement connu sous le nom de l’Esthète, s'employant à "rendre immortel ceux qui permettent le monde d'être plus beau". De plus, aujourd'hui, le marché regorge de sang animaux, même déshydraté. Les vampires restent cependant la cible numéro 1 des chasseurs.

Les vampires vivent entre eux, généralement en société savantes et culturelles. Cette façon de vivre est en grande partie liée à l'Esthète.

## Ghoules

Une ghoule est un "semi-mort vivant", un être transformé par un vampire temporairement et sous son contrôle.

## Dhampire

Des demi-vampire, des Dhampire, peuvent exister si un être est vampirisé durant son processus de gestation (plante danaïte, grossesse, oeuf...). Ils vieillissent normalement et ont l'espérence de vie de leur espèce originelle, mais ont toutes les autres caractéristiques du vampire.

## Deretour

Les deretour sont le terme générique utilisé pour désigner tout les morts-vivantsw
