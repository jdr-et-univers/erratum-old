---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les signes
  key: Les vertus
  order: 5
---

# Les vertus

Les vertus sont sept êtres existant depuis des temps immémoriaux. Le rôle de vertue se retransmet de personne à personne à la mort d'une vertue, une nouvelle personne naissant et devenant vertue. Chacune - sauf la vertue de la tempérence, ont deux signes. Les vertues peuvent également le retransmettre volontairement à une personne en sentant leur fin approcher.

Chaque vertu possède une arme unique, leur permettant des exploits incroyables. Elles se repèrent par les lignes de lumières traversant tout leur corps, chacune d'une couleur différente. Elles peuvent briller fort lorsque la vertu utilise ses pouvoirs où dans des émotions trop intense. Une vertu ayant ses pouvoirs actif est dite *exhaltée*

Elles avait disparue à la fin de la *grande guerre des vertues*, mais ont été recrée par les cultiste au début du XXIe siècle, leur pouvoir ayant été mise dans sept orphelins afin de les érigées en protecteur et protectrices du monde.

| Nom vertu | Signes | Arme de vertu |
|:----------:|:------:|:------------:|
| Bravoure   | Taureau / Lion | Son propre corps. |
| Confiance  | Vierge / Cancer | Le Grand Caducée            |
| Espérence  | Poisson / Scorpion | Les sabres de la dualité |
| Justice    | Saggitaire / Balance | L'arc de la justice |
| Charité    | Bélier / Gémeaux | Les instruments de l'harmonie |
| Prudence   | Verseau / Capricorne | La montre des possibles |
| Tempérence | Serpentaire | ????? |
