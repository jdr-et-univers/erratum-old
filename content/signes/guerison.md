---
layout: layouts/base.njk
eleventyNavigation:
  parent: Les signes
  key: Arcane de la Guérison
  order: 0
---

# Arcane de la Guérison

*La guérison* représente le pouvoir de guérir, améliorer la situation des gens autour de soi. Elle peut aussi représenter le partage, le don et l'aide apporté à ceux qui nous entoure.

Ses signes sont le cancer, la vierge et la balance.

## Le Cancer (le phénotype)

Le signe cancer est un signe rare offrant le pouvoir de contrôler les phénotypes et génotypes, à échelle plus ou moins grande. Ils modifient directement les structures biologiques à leur avantage, pouvant en faire une arme comme une manière d'aider.

Cela dit, cela rend aussi leur soins dangereux pour ceux qui le subissent. Ils peuvent créer des soins particulièrement performant au prix d'effet secondaire redoutable. Il s'agit d'un signe rare et puissant.

Leur hérault est **Avicenne** (Ibn Sina), fondateur de la médecine moderne, entre le 10e et 11e siècle. Son temple est situé à Hamadan, en Iran.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :cancer: | Rat (asie) | Guérison et Force (Soigneur DPS) | Chaos | Rare (01.2%) | INT et HAB | Gris |

## Vierge (confiance)

Les vierges tirent leur pouvoir de la foi, de la confiance qu'elles peuvent accorder ou qu'on peut leur accorder. Il ne s'agit pas d'une confiance qu'elle donne forcément à l'heure actuelle, mais d'une qu'on pourrait ou aimerait donner. Cette confiance peut être envers une personne, un concept abstrait, etc.

Cette puissance leur offre à la fois protection et capaciter à aider leur prochain. Mais il s'agit d'une protection toute particulière : il est plus simple de se mettre en danger voir de se sacrifier quand on a confiance envers quelqu'un...

Leur hérault est **Bakhna Rakhna** première des Yumboes, une ruche danaïte d'Afrique de l'Ouest, amies des âmes défuntes, au 7e siècle. Son temple est situé dans la ruche des Yumboes, sur la péninsule du Cap Vert au Sénégal.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :virgo: | Cochon (asie), Isis (égypte) | Guérison et Protection (Soigneur Tank) | Ordre | Peu commun (07.0%) | VOL, SAG et REL | Rose |

## Balance (justice et equilibre)

Les balances tirent leur pouvoir de l'équilibre : que celui-ci soit entre les situations, les valeurs numériques ou les énergies qui traversent le corps. Ce pouvoir en font donc de parfais représentants des arcanes de la guérison et de la malice. Elles peuvent manipuler les énergies vitales, les redistribuer, mais également manipuler certaines quantités autour d'elles, notamment la chance.

On rapproche pour ce fait de l'arcane de la justice, avec l'ide de manipuler les gains et les blessures.

Leur hérault est **Salomon**, l'antique roi sage dont la justice est restée célèbre, vers -950. Son temple est situé à Jérusalem.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :libra: | Chien (asie), Maât (égypte) | Guérison et malice (Healer Trickster) | Ordre | Peu commun (05.5%) | SAG et INT | Vert |
