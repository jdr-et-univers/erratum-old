---
layout: layouts/base.njk
eleventyNavigation:
  key: Les signes
  order: 4
---

# Les signes

Les « signes » sont le nom donné à une magie naturelle qui apparaît chez environs 0.7 % de la population mondiale. Cette magie est rare, et les signés ont toujours eu la possibilité d’avoir une influence non-nulle sur les événements de par cette magie

Il est toujours inconnu de si le signe est inné ou acquis, mais il se déclenche de manière incontrôlable à un moment de la vie. Il y a souvent un lien entre le signe et la personnalité de quelqu'un, mais des théories différente existent pour savoir lequel influence lequel (beaucoup d'alchimiste répondent : "c'est compliqué, sans doute un peu des deux").

Les signes sont divisés en quatre arcanes, deux alignements.

## Arcanes et alignement

Les arcarnes sont une division des signes suivant quatre grand principe : la force, la guérison, la protection et la malice, qui représente les types de pouvoirs suivants :

- *La guérison* représente le pouvoir de guérir, améliorer la situation des gens autour de soi. Elle peut aussi représenter le partage, le don et l'aide apporté à ceux qui nous entoure.
- *La force* représente la puissance, l'attaque, la capacité brute d'action. Cette arcane est souvent dangereuse, mais n'est pas forcément qu'une arme : un couteau peut servir à attaquer, mais aussi d'outil.
- *La protection* représente le pouvoir de défendre son entourage des menace, de garantir la sécurité et le bien-être de tous. Cela peut être une protection physique, mentale, ou métaphorique (tel qu'une protection contre les maux).
- *La malice* représente le pouvoir de manipuler, de changer, de transformer. Elle est souvent l'arcane apportant le plus de surprise, bonne comme mauvaise, de par son pouvoir de changement.

Lorsque quelqu'un obtient plusieurs signes, le second signe doit avoir une arcane en commun avec le premier signe, et le dernier devra avoir cette arcane aussi - si la personne atteint les trois signes bien sûr, un phénomène extrèmement rare.

## Alignement

Chaque signe possède un alignement, qui influencera une affinité à un élément métaphysique :

- Le *chaos* représentent les signes dont les pouvoirs possède la plus grande part de probabilité, d'effets secondaires. Elles sont proche des forces du chaos, maniée généralement par les démons, les diablons et la chance.
- L'*ordre* représente les signes dont les effets sont les plus stables. Elles sont proches des puissancces de l'ordre, maniée par les anges, certains dieux et le cosmos.

Il est possible pour les multi-signés de possédé des signes de plusieurs alignement.

## Apothéose

L'apothéose est le phénomène mystérieux pouvant faire apparaitre un signe chez une personne. Si le premier signe apparait souvent sans apothéose, naturellement, le second voir troisième signe ne peut apparaitre qu'après celle-ci.

Ce phénomène se produit dans des circonstantes très particulières, qui peuvent être les suivantes :
- Quand une personne surpasse tout ce qu'elle est, atteint son summum, dans les moments ou "on est le plus nous-même".
- Aux portes de la mort, lorsque quelqu'un est conduit à ce moment fatidique par des actions le représentant.
- Très rarement, des situations extrèmes peuvent aussi le provoquer.

La personne en apothéose se réveille alors dans un lieu entièrement sombre, faisant partie du *noumène* et se retrouve en discussion avec Lux, l'Arbitre. Celui-ci cependant ne choisira pas le signe.

Les règles de gains de signe sont les suivantes :
- Le premier signe est légèrement déterminé par la personnalité de l'individu
- Le second signe est limité par uniquement les signes ayant une arcane en commun avec celui déjà présent
- Le troisième signe doit avoir l'arcane commune des deux premier signes.

Il est possible de reprendre une fois un signe déjà acquis, permettant de le *doubler*.

## Herault

Les *hérault* des signes sont des êtres ayant poussé leur signe extrèmement loin. On raconte qu'ils possèdent tous quelque part dans le monde un temple qui permettrait de débloquer les secrets cachés de son signe.

En plus des 12 hérault, il existe quatre **hérault d'Arcane** ou **Grand-Hérault** sont quatre être ayant maitrisé tout les aspect d'une arcane, et pouvant donc offrir des nouveaux pouvoir à un⋅e manipulateur⋅ice de ladite arcane. Ces quatre êtres sont à la limite entre divinité et humanité. Ils sont aux nombres de 4. Les quatres grands-herault sont les suivants :

- Le hérault de la Guérison est **Nokomis**, aussi nommée Grand-Mère ou Terre-Mère, femme dont la puissance de la nature étant grande. Son temple est situé en Amérique du Nord.

- Le hérault de la Force est **Gilgamesh**, le roi-héros, aux exploits légendaires qui ont transcendé l'histoire et les légendes, vers -3000. Son temble est situé à Babylone.

- Le hérault de la Protection est **Hervor**, grande héroïne Skjaldmö (guerrières nordiques armées d'un bouclier) ayant pu jusqu'à rivaliser avec les valkyrie. Son temple est située en scandinavie.

- Le hérault de la Malice est **Maui**, grand trickster et protecteur des hommes en polynésies et dans tout le pacifique. Son temple est situé à Hawaiiki.
